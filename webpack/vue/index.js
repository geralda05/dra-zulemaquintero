import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import index from './views/index.vue'
import inicio from './views/templates/inicio/inicio.vue'
import personalinfo from './views/templates/personalinfo/experiencia.vue'
import servicios from './views/templates/servicios/servicios.vue'
import contacto from './views/templates/contacto/contact.vue'
import error404 from './views/templates/errores/404.vue'


const router = new VueRouter({
    mode:'history',
    routes:[
        {
            path:'/',
            redirect: "/inicio",

        },
        {
            path:'/inicio',
            name:"inicio",
            component: inicio,
            props:true,
        },
        {
            path:'/personalinfo',
            name:"personalinfo",
            component: personalinfo,
        },
        {
            path:'/servicios',
            name:"servicios",
            component: servicios,
        },
        {
            path:'/contacto',
            name:"contacto",
            component: contacto,
        },
        {
            path:'/404',
            name:"error-404",
            component: error404,
        },
        {
            path:"*",
            redirect:"/404",
        }
    ]
});

const app = new Vue({
    router,
    render: createEle => createEle(index)

}).$mount('#app');