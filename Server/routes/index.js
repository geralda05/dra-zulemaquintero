var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Este es el ejemplo de una ruta hecha por Gerald Alarcon');
});

module.exports = router;
