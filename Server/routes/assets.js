var express = require('express');
var app = express();

assets = [

    app.use('/assets/bootstrap/js', express.static(__dirname + '/../node_modules/bootstrap/dist/js')),
    app.use('/assets/bootstrap/css', express.static(__dirname + '/../node_modules/bootstrap/dist/css')),
    app.use('/css', express.static(__dirname + '/../public/stylesheets')),
    app.use('/img', express.static(__dirname + '/../public/images')),
    app.use('/media', express.static(__dirname + '/../public/media')),
],

module.exports = assets;